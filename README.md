# New-Movies-In-Cinema

This app shows the movies that are now playing in cinema.
The app is using an MVP pattern.
Selecting a movie will show a detail view of the movie.
If a movie is part of a collection of movies, from the detail view you will be able to see the other movies in the collection and selecting them will link through to the detail view for that movie.
Testing coverage is around 67%.
